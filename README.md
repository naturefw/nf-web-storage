# nf-web-storage 前端存储的操作库

> 最新代码转移到： https://gitee.com/naturefw-code/nf-rollup-webstorage  
源码目录：https://naturefw.gitee.io/  

## 介绍
在 vue3 的环境下对前端存储进行封装，便于使用。  
基于 promise 封装 indexedDB、webSQL。  
以及对 localstorage、localsession的封装。

## 技术栈
* vue3
* vite2
* indexedDB 
* webSQL


## 安装教程

```
yarn add nf-web-storage
```

## 使用说明
包含三个封装类库：indexedDB 的 help、webSQL 的 help 以及 localStorage 的 help

#### websql  

* 定义对象仓库
```js

// 访问 indexedDB
import { dbCreateHelp } from '../../packages/storage.js'

// 引入数据库数据
const db = {
  dbName: 'nf-indexedDB-test',
  ver: 1
}

/**
 * 客户项目的 meta 的 db 缓存
 */
export default function setup (callback) {
  const install = dbCreateHelp({
    // dbFlag: 'project-meta-db',
    dbConfig: db,
    stores: { // 数据库里的表
      moduleMeta: { // 模块的meta {按钮,列表,分页,查询,表单若干}
        id: 'moduleId',
        index: {},
        isClear: false
      },
      menuMeta: { // 菜单用的meta
        id: 'id',
        index: {},
        isClear: false
      },
      serviceMeta: { // 后端API的meta，在线演示用。
        id: 'moduleId',
        index: {},
        isClear: false
      },
      testIndex: { // 演示一下索引的查询。
        id: 'moduleId',
        index: {
          kind: false,
          type: false
        },
        isClear: false
      }
    },
    // 加入初始数据
    init (help) {
      if (typeof callback === 'function') {
        callback(help)
      }
    }
  })
  return install
}

```

* main.js 里面调用，初始化

```js

// 引入 indexedDB 的 help
import dbHelp from './store-project/db.js'

dbHelp((help) => {
  // indexedDB 准备好了
  console.log('main里面获取 indexedDB 的help', help)
})

```

* 组件里面使用

```js
import { useStores, useDBHelp } from 'nf-web-storage'

const { menuMeta } = useStores()
 

// 添加对象的测试
const add = () => {
  const t1 = window.performance.now()
  console.log('\n -- 准备添加对象 --：', t1)
  const model = {
    id: new Date().valueOf(),
    name: 'test-在vue里面使用。'
  }
  menuMeta.add(model).then((res) => {
    const t2 = window.performance.now()
    console.log('添加成功！', res, '用时：', t2 - t1, '\n')
  
  })
}

```


## 源码

[![自然框架/nf-web-storage](https://gitee.com/naturefw/nf-web-storage/widgets/widget_card.svg?colors=ffffff,1e252b,323d47,455059,d7deea,99a0ae)](https://gitee.com/naturefw/nf-web-storage)

## 相关源码
https://naturefw.gitee.io/ 

## 在线演示

https://naturefw.gitee.io/vite2-vue3-demo 


## 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
