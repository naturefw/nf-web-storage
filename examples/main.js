import { createApp } from 'vue'
import App from './App.vue'

// 封装 webSQL 的操作类
import store_websql from './store_websql'

createApp(App)
  .use(store_websql)
  .mount('#app')
