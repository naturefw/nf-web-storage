import { webSQLVue } from '../../packages/index'

/**
 * 打开数据库，建立表，对表建立实例。
 */
export default webSQLVue.createHelp({
  dbConfig: { // 连接数据库
    dbName: 'test-vue',
    ver: 1.0,
    remarks: '测试封装webSQL的操作函数',
    size: 2
  },
  tables: { // 数据库里的表
    blog: { // 对象属性，建立表的依据，对象名作为表名，属性名作为字段名，属性值表示注释
      id: 0,
      title: '这是一个博客标题',
      addTime: '添加时间',
      introduction: '这是博客的简介',
      viewCount: '浏览量',
      agreeCount: '点赞数量' 
    },
    discuss: { // 可以设置多个表
      id: 0,
      discusser: '讨论人',
      addTime: '添加时间',
      concent: '讨论内容',
      agreeCount: '点赞数量'
    }
  },
  isDeleteOldTable: true, // 初始化的时候是否删除之前的表
  // 可以给全局状态设置初始状态，同步数据可以直接在上面设置，如果是异步数据，可以在这里设置。
  init(help) {
    // 可以做一些事情，比如添加初始数据
    help.insert('blog', {
      title: '初始化数据',
      addTime: new Date().valueOf(),
      // introduction: '这是博客的简介',
      // viewCount: 0,
      agreeCount: 0
    }).then((id) => {
      console.log('init help 的方式添加记录，ID：',id)
    })
  }
}) 