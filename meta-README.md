# nf-meta meta的管理类

## 介绍
自己使用的。

## 技术栈
* vue3
* vite2


## 安装教程

```
yarn add nf-meta
```

## 使用说明


## 源码

[![自然框架/nf-web-storage](https://gitee.com/naturefw/nf-web-storage/widgets/widget_card.svg?colors=ffffff,1e252b,323d47,455059,d7deea,99a0ae)](https://gitee.com/naturefw/nf-web-storage)

## 相关源码
https://naturefw.gitee.io/ 

## 在线演示

https://naturefw.gitee.io/vite2-vue3-demo 


## 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
