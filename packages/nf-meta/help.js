import { reactive } from 'vue'

// 从json文件加载meta
import loadMetaFormJson from './loadmeta-json.js'

// 从 webSQL加载meta
import loadMetaFormSQL from './loadmeta-sql.js'

// 把meta存入 indexedDB
import { saveMetaAll, loadMeta } from './savemeta-db.js'

/**
 * meta的help
 * * 从 json 加载 meta，存入 indexedDB
 * * 从 webSQL 加载 meta，存入 indexedDB
 * * 从 indexedDB 加载 meta，
 * * 把 meta 存入状态
 */
export default class MetaHelp {
  constructor (url) {
    this.jsonUrl = url, // json 的路径，axios 加载用
    this.sqlHelp = null, // 访问 webSQL
    this.dbHelp = null, // 访问 indexedDB
    this.meta = reactive({
      menu: {},
      module: {},
      service: {}
    })
  }

  /**
   * 从 indexedDB 里面加载 meta
   * @returns 
   */
  async loadMetaFromDB() {
    return await loadMeta(this.dbHelp)
  }

  /**
   * 把 meta 存入 state
   * @param {*} state 状态
   * @param {*} meta 要存入的 meta。menu：数组；module：对象；service：对象
   */
  toState(state, meta) {
    // 存入菜单，需要去重
    meta.menu.forEach(_menu => {
      if (state.menu.findIndex((m) => m.id === _menu.id) < 0) {
        state.menu.push(_menu)
      }
    })
    // 存入模块
    Object.assign(state.module, meta.module)
    // 存入服务
    Object.assign(state.service, meta.service)
  }

  /**
   * 从 webSQL 加载 meta，然后存入 indexedDB，并且返回 meta
   * @param {*} moduleId 
   */
  async sqlToDB(moduleId = null) {
    console.log('MetaHelp 的 sqlToDB', this)
    const meta = await loadMetaFormSQL(this.sqlHelp)
    console.log('MetaHelp 的 SQL 读取出来的 meta ', meta)

    // 遍历，添加到 indexedDB
    saveMetaAll(this.dbHelp, meta)

    return meta
  }

  /**
   * 从 json 加载 meta，然后存入 indexedDB，并且返回 meta
   * @param {*} moduleId 模块ID
   * @returns 
   */
   
  async loagMetaFromJson(moduleId = null) {
    console.log('MetaHelp 的 sqlToDB', this)
    const meta = await loadMetaFormJson(this.jsonUrl)
    console.log('MetaHelp 的 从json加载的meta：', meta)

    // 遍历，添加到 indexedDB
    saveMetaAll(this.dbHelp, meta)
    return meta
  }
}