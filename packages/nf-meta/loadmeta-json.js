
// axios
import axios from 'axios'

/**
 * 加载 json 文件 整理成 meta 的格式，返回
 */

/**
 * 01 获取 json文件 的目录，因为json文件比较多，还希望实现热更新，所以做了个加载目录。
 * * 应该加个随机数，便于即使更新。
 * * 因为有缓存，所以不用怕过多访问的问题。
 */
const _loadFileDirectory = (jsonURL) => {
  return new Promise((resolve, reject) => {
    const _url = `${jsonURL}/dir.json?v=1`
    axios.get(_url).then((res) => {
      // console.log('dir -- 目录：', res)
      resolve(res.data)
    }).catch((res) => {
      reject(res)
    })
  })
}

/**
 * 整理加载的json，转成meta格式
 */
const _format = (res) => {
  const meta = {
    service: {},
    module: {},
    menu: res[0].data.menu
  }

  // 遍历meta，进行分类
  res.forEach((re, index) => {
    if (index === 0) return // 第一个是菜单，
    const model = re.data
    if (typeof model.actions !== 'undefined') {
      // 后端API
      meta.service[model.moduleId] = model
    } else {
      // 模块的 meta
      if (typeof meta.module[model.moduleId] === 'undefined') {
        meta.module[model.moduleId] = {
          moduleId: model.moduleId
        }
      }

      // 开始判断
      if (typeof model.btnOrder === 'object') {
        // 按钮
        meta.module[model.moduleId].button = model
      } else if (typeof model.quickFind === 'object') {
        // 查询
        meta.module[model.moduleId].find = model
      } else if (typeof model.idName === 'string') {
        // 列表
        meta.module[model.moduleId].grid = model
      } else if (typeof model.formId !== 'undefined') {
        // 表单
        if (typeof meta.module[model.moduleId].forms === 'undefined') {
          meta.module[model.moduleId].forms = {}
        }
        meta.module[model.moduleId].forms[model.formId] = model
      }
    }
  })
  return meta
}

/**
 * axios 读取json文件，然后返回
 * @param {*} josnDir 前端模块meta 的文件夹
 * @param {*} serviveDir 后端服务meta 的文件夹
 * @param {*} jsonURL 项目meta的url
 * @returns 
 */
const _loadMeta = (josnDir, serviveDir, jsonURL) => {
  return new Promise((resolve, reject) => {
    const actionName = ['button', 'find', 'grid']
    // 加载json的 请求的 数组，交给 Promise.all 使用
    const getMetaAxios = []
    // 加入导航菜单
    const _url = `${jsonURL}/menu.json`
    getMetaAxios.push(axios.get(_url))

    // 前端meta 请求 加入数组
    for (const key in josnDir) {
      const meta = josnDir[key] // 模块需要的表单
      // 添加按钮、列表、查询
      actionName.forEach(action => {
        const _url = `${jsonURL}module/${key}/${action}.json`
        getMetaAxios.push(axios.get(_url))
      })
      // 添加表单
      meta.forEach(form => {
        const _url = `${jsonURL}module/${key}/${form}.json`
        getMetaAxios.push(axios.get(_url))
      })
    }

    // 后端API的meta 请求，也加入数组
    serviveDir.forEach(api => {
      const _url = `${jsonURL}service/${api}.json`
      getMetaAxios.push(axios.get(_url))
    })

    // 一起发起所有json文件的请求
    Promise.all(getMetaAxios).then((res) => {
      // console.log('data:', res)
      if (res[0].status === 200) { // statusText
        // json 数据 转换成 meta 格式
        const meta = _format(res)
        console.log('----json--处理好后的--meta：--', meta)
        // 返回 meta
        resolve(meta)
      }
    })
  })
}

/**
 * 加载josn文件，整理后变成meta格式
 * @param {string} jsonURL 项目meta的url
 * @returns 整理好的meta
 */
const loadMetaFormJson = async (jsonURL) => {
  // 获取json的文件目录，便于 axios 加载
  const dir = await _loadFileDirectory(jsonURL)
  return await _loadMeta(dir.jsonDir, dir.serviceDir, jsonURL)
}

export default loadMetaFormJson
