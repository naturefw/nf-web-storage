/**
 * 从 SQL 里面加载数据，整理成 meta 的格式
 */

/**
 * 把 webSQL 里的【菜单】数据变成 meta 的格式
 */
const _getMenuMetaBySQL = (data) => {
  const menu = []
  data.forEach(m => {
    menu.push({
      id: m.moduleId,
      componentKind: m.componentKind,
      icon: m.icon,
      moduleLevel: m.moduleLevel,
      parentId: m.parentId,
      title: m.title
    })
  })
  return menu
}

/**
 * 把 webSQL 里的【按钮】数据变成 meta 的格式
 */
const _getButtonMetaBySQL = (data) => {
  const meta = {
    moduleId: data.moduleId,
    btnOrder: [],
    itemMeta: {}
  }
  data.forEach(btn => {
    meta.btnOrder.push(btn.buttonId)
    meta.itemMeta[btn.buttonId] = btn
  })
  return meta
}

/**
 * 把 webSQL 里的【列表】数据变成 meta 的格式
 */
const _getGridMetaBySQL = (grid, item) => {
  const meta = grid[0]
  meta.colOrder = meta.colOrder.split(',')
  meta.itemMeta = {}
  // meta.quickFind = meta.quickFind
  item.forEach(ctl => {
    // meta.colOrder.push(ctl.columnId)
    meta.itemMeta[ctl.columnId] = ctl
  })
  return meta
}

/**
 * 把 webSQL 里的【查询】数据变成 meta 的格式
 */
const _getFindMetaBySQL = (find, item) => {
  const meta = find[0]
  meta.allFind = meta.allFind.split(',')
  meta.quickFind = meta.quickFind.split(',')
  meta.itemMeta = {}
  item.forEach(ctl => {
    // meta.quickFind.push(ctl.columnId)
    // meta.allFind.push(ctl.columnId)
    meta.itemMeta[ctl.columnId] = ctl
  })
  return meta
}

/**
 * 把 webSQL 里的【表单】数据变成 meta 的格式
 * * form 数组，包含多个 formId
 */
const _getFormMetaBySQL = (forms, items) => {
  const meta = {}
  forms.forEach(fm => {
    meta[fm.formId] = fm
    meta[fm.formId].colOrder = meta[fm.formId].colOrder.split(',')
    meta[fm.formId].itemMeta = {}
    items.filter(a => a.formId === fm.formId).forEach(ctl => {
      meta[fm.formId].itemMeta[ctl.columnId] = ctl
    })
  })
  return meta
}

/**
 * 把 webSQL 里的【actions】数据变成 meta 的格式
 */
const _getActionMetaBySQL = (data) => {
  const actions = {}
  data.forEach(action => {
    actions[action.actionId] = {
      actionName: action.actionName,
      kind: action.kind,
      model: action.modelId
    }
  })
  return actions
}

/**
 * 把 webSQL 里的【models】数据变成 meta 的格式
 */
const _getModelMetaBySQL = (data) => {
  const models = {}
  data.forEach(model => {
    models[model.actionId] = {
      tableName: model.tableName,
      idKey: model.idKey,
      cols: model.cols,
      pager: {
        orderBy: { roleId: false },
        pagerIndex: 1,
        pagerSize: model.pagerSize,
        pagerTotal: 100
      },
      query: {}
    }
  })
  return models
}

/**
 * 开发模式：从 webSQL 读取数据，转换成meta 的格式
 * * 菜单meta、模块meta、service的meta
 * * 从 SQL 加载数据，转换格式，返回。
*/
const loadMetaFormSQL = async (sqlHelp) => {
  console.log('------webSQL 的 sqlHelp', sqlHelp)

  const tables = sqlHelp._tables

  // 存放 SQL 里面的 meta
  const meta = {}
  // 获取所有数据库里的 meta 数据
  for (const key in tables) {
    const tt = tables[key]
    const tmp = await tt.list()
    meta[key] = Array.from(tmp)
  }
  console.log('------controller 的 meta', meta)

  // 返回的 meta
  const reMeta = {
    menu: _getMenuMetaBySQL(meta.nf_module), // 记录菜单
    module: {}, // 模块
    service: {} // service
  }

  const _tmp = (name, modId) => {
    return meta[name].filter((a) => a.moduleId === modId)
  }

  // 遍历菜单，变成 meta 的格式
  for (let i = 0; i < meta.nf_module.length; i++) {
    const modId = meta.nf_module[i].moduleId
    const __moduleMeta = { // 记录模块的meta
      moduleId: modId,
      pager: {},
      button: {},
      grid: {},
      find: {},
      forms: {}
    }
    
    // 分页
    // __moduleMeta.pager = this._getButtonMetaBySQL(meta[151].filter((a) => a.moduleId === modId))
    // 列表
    __moduleMeta.grid = _getGridMetaBySQL(_tmp('v_module_grid', modId), _tmp('v_module_grid_item', modId))
    // 按钮
    __moduleMeta.button = _getButtonMetaBySQL(_tmp('v_module_button', modId))
    // 查询
    __moduleMeta.find = _getFindMetaBySQL(_tmp('v_module_find', modId), _tmp('v_module_find_item', modId))
    // 表单
    __moduleMeta.forms = _getFormMetaBySQL(_tmp('v_module_form', modId), _tmp('v_module_form_item', modId))

    // 记录
    reMeta.module[modId] = __moduleMeta
  }

  // 遍历service，整理后端api
  for (let i = 0; i < meta.v_service.length; i++) {
    const service = meta.v_service[i]
    const serviceId = service.serviceId
    // 记录service
    const serviceMeta = {
      moduleId: serviceId,
      moduleName: service.serviceName,
      actions: {},
      models: {}
    }
    const _tmp = (id) => {
      return meta[id].filter((a) => a.serviceId === serviceId)
    }
    serviceMeta.actions = _getActionMetaBySQL(_tmp('v_service_action', serviceId))
    serviceMeta.models = _getModelMetaBySQL(_tmp('v_service_model', serviceId))

    reMeta.service[serviceId] = serviceMeta
  }

  // 返回整理好的 meta
  return reMeta
}

export default loadMetaFormSQL
