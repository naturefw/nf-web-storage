/**
 * saveMeta：把 meta 的一个属性 存入 indexedDB 的一个对象仓库
 * saveMetaAll：把 meta 存入 indexedDB
 * loadMeta：从 indexedDB 加载meta
 */

/**
 * json、webSQL 的 meta，存入 indexedDB
 * @param {help} help 访问 indexedDB 的 help
 * @param {string} storeName 仓库名称
 * @param {object} meta 要存入的对象，对象集合
 * @returns 仅通知
 */
 const saveMeta = (help, storeName, meta) => {
  const idName = { // 主键名称的字典
    menuMeta: 'id',
    moduleMeta: 'moduleId',
    serviceMeta: 'moduleId'
  }

  return new Promise((resolve, reject) => {
    let count = 0
    help.beginInit(storeName).then((store) => {
      count += Object.keys(meta).length
      if (count === 0) {
        resolve(null)
      }
      for (const key in meta) {
        // 先判断有没有，没有add；有了put
        store.get(meta[key][idName[storeName]])
          .onsuccess = (event) => {
            // console.log('====== 要添加的key：', storeName + '_' + meta[key][idName[storeName]])
            // console.log('====== 获取的 结果：', event.target.result)
            if (typeof event.target.result === 'undefined') {
              // 添加
              store.add(meta[key]) // 添加一条meta
                .onsuccess = (event) => {
                  count -= 1
                  if (count === 0) {
                    resolve(event.target.result)
                  }
                }
            } else {
              // 修改
              store.put(meta[key]) // 修改一条meta
                .onsuccess = (event) => {
                  count -= 1
                  if (count === 0) {
                    resolve(event.target.result)
                  }
                }
            }
          }
      }
    })
  })
}

/**
 * 把 meta 存入 indexedDB
 * @param {*} help indexedDB 的 help
 * @param {*} meta 要存入的 meta
 */
const saveMetaAll = async (help, meta) => {
  await saveMeta(help, 'serviceMeta', meta.service)
  await saveMeta(help, 'moduleMeta', meta.module)
  await saveMeta(help, 'menuMeta', meta.menu)
}

/**
 * 从 indexedDB 里面加载 meta，并且返回
 * @param {*} help indexedDB 的 help
 * @returns 
 */
const loadMeta = async (help) => {
  const state = {
    menu: [],
    module: {},
    service: {}
  }
  state.menu = await help.getModel('menuMeta')
  const _module = await help.getModel('moduleMeta')
  const _service = await help.getModel('serviceMeta')

  for (const key in _module) {
    const m = _module[key]
    state.module[m.moduleId] = m
  }
  for (const key in _service) {
    const s = _service[key]
    state.service[s.moduleId] = s
  }
  return state
}

export {
  saveMetaAll, // 存入 meta
  saveMeta, // 存入一个属性
  loadMeta // 加载 meta
}
