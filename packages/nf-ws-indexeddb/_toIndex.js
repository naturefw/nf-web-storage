
/**
 * 一般查询方式的字典
 */
const _find = {
  401: (colValue, key) => colValue === key, // =
  403: (colValue, key) => colValue.includes(key), // 包含
  405: (colValue, key) => colValue.indexOf(key) === 0, // 起始于
  406: (colValue, key) => colValue.indexOf(key) + key.length === colValue.length, // 结束于
  413: (colValue, key) => colValue > key, // >
  414: (colValue, key) => colValue >= key, // >=
  415: (colValue, key) => colValue < key, // <
  416: (colValue, key) => colValue <= key, // <=
  417: (colValue, key) => key[0] <= colValue && colValue <= key[1], // between
  418: (colValue, key) => key[0] < colValue && colValue <= key[1], // a < x <= b
  419: (colValue, key) => key[0] <= colValue && colValue < key[1], // a <= x < b
  420: (colValue, key) => key[0] < colValue && colValue < key[1] //  a < x < b

}

/**
 * IDBKeyRange 的字典
 */
const _dicRange = {
  401: (_value) => IDBKeyRange.only(_value), // =
  // 403: (_value) => IDBKeyRange.only(_value),
  // 405: (_value) => IDBKeyRange.only(_value),
  // 406: (_value) => IDBKeyRange.only(_value),
  413: (_value) => IDBKeyRange.lowerBound(_value, true), // >
  414: (_value) => IDBKeyRange.lowerBound(_value), // >=
  415: (_value) => IDBKeyRange.upperBound(_value, true), // <
  416: (_value) => IDBKeyRange.upperBound(_value), // <=
  417: (_value) => IDBKeyRange.bound(_value[0], _value[1]), // between
  418: (_value) => IDBKeyRange.bound(_value[0], _value[1], true, false), // a < x <= b
  419: (_value) => IDBKeyRange.bound(_value[0], _value[1], false, true), // a <= x < b
  420: (_value) => IDBKeyRange.bound(_value[0], _value[1], true, true) //  a < x < b
}

// 索引的排序方式，正序、倒序
// const _description = page.description || 'prev' // 默认倒序

/**
 * 遍历查询条件，找到对应的字段，做判断，有一个不符合就返回 false
 * @param {*} other 查询条件
 * @param {*} model 要核对的对象
 * @returns 是否符合
 */
const _check = (other, model) => {
  let re = true
  for (const key in other) {
    const colValue = model[key] // 被查询的内容
    const kind = other[key][0] // 查询方式
    const _key = other[key][1] // 查询条件的值
    const _re = _find[kind](colValue, _key) // 验证
    if (!_re) re = false
  }
  return re
}

/**
 * 处理索引和其他查询
 * @param {*} indexNames 对象仓库已经设置的索引
 * @param {*} query 查询条件
 * @returns IDBKeyRange、其他查询条件、查询方式、回调查询
 */
const _toIndex = (indexNames, query) => {
  const re = {
    range: null,
    other: {},
    find: (other, model) => _check(other, model), // 查询非索引字段
    callback: () => {} // 回调查询，可以自定义查询方式
  }

  // 匹配到的索引字段
  let indexName = ''

  // 查询条件字段和索引字段，匹配一下，只匹配第一个索引字段
  for (let i = 0; i < indexNames.length; i++) {
    // 索引字段名称
    const _indexName = indexNames[i]

    if (typeof query[_indexName] !== 'undefined') {
      // 查询方式
      const _kind = query[_indexName][0]
      // 查询的值
      const _value = query[_indexName][1]
      // 查询条件里包含索引，记录，后面的就不管了。
      // 匹配查询方式
      re.range = _dicRange[_kind * 1](_value)
      // 没有的话，说明无法利用索引
      if (typeof re.range === 'undefined') {
        re.range = null
      } else {
        indexName = _indexName
      }
    }

    if (indexName !== '') {
      // 退出循环
      i = indexNames.length
    }
  }

  // 把其他的查询字段放在 other 里面
  for (const key in query) {
    const _query = query[key]
    if (indexName !== key) {
      // 记录到其他查询条件里面
      re.other[key] = _query
    }
  }

  return re
}

export default _toIndex
