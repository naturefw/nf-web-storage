import { unref, isReactive, toRaw } from 'vue'

// 内部函数，如果是proxy，那么获取原型，否则会报错。
const _vueToObject = (obj) => {
  /*
  // 处理 ref
  let _object = unref(obj)
  // 处理 reactive
  if (isReactive(_object)) {
    // 如果是 vue 的 reactive 类型，那么获取原型，否则会报错
    _object = toRaw(_object)
  }
  */
  const tmp = unref(obj)
  const _object = isReactive(tmp) ? toRaw(tmp): tmp
  return _object
}

export default _vueToObject
