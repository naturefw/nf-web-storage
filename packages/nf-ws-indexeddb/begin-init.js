/**
 * 初始化数据的时候使用的一个事务。
 * * help._db 必然可用，不用判断。
 */
const beginInit = (help, storeName) => {
  return new Promise((resolve, reject) => {
    //
    const tranRequest = help._db.transaction(storeName, 'readwrite')
    const store = tranRequest.objectStore(storeName) // 获取store

    tranRequest.onerror = (event) => {
      console.log('读写事务出错：', event.target.error)
      // eslint-disable-next-line prefer-promise-reject-errors
      reject('读写事务出错：' + event.target.error)
      // tranRequest.abort() // 大概是回滚的意思。
    }

    tranRequest.oncomplete = (event) => {
      // console.log('初始化数据事务完毕：', window.performance.now(), help._dataState)
      // help._dataState = 'done'
    }
    resolve(store)
  })
}

export default beginInit
