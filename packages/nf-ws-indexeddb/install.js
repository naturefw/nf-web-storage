import { reactive } from 'vue'
// 引入 indexedDB 的help
import IndexedDB from './help.js'

/**
 * 对 indexedDB 的 help 进行初始化
 */
export default {
  _indexedDBFlag: Symbol('nf-indexedDB-help'),
  _help: {}, // 访问数据库的实例
  _stores: {}, // 存放对象，实现 foo.addModel(obj)的功能
  /**
   * 根据参数创建一个数据库的实例，初始化数据库
   * * 删表、建表、添加默认数据
   * @param {*} info 参数
   * @returns
   * * dbFlag: '数据库标识，区分多个数据库',
   * * dbConfig: { // 连接数据库
   * * *  dbName: 'vite2-blog',
   * * *  ver: 1.0
   * * },
   * * init: () => {}, // 初始化完成后的回调函数
   * * stores: {
   * * * storeName: { // 对象仓库名
   * * * * id: 'id', // 主键名称
   * * * * index: {
   * * * * * name: ture, // 索引：是否可以重复
   * * * * },
   * * * * isDeleteOldTable: false, // 是否删除之前的对象仓库
   * * * }
   * * }
   */
  createHelp (info) {
    const indexedDBFlag = typeof info.dbFlag === 'undefined' ?
      this._indexedDBFlag : info.dbFlag
   
    // 连接数据库，获得实例。
    const help = new IndexedDB(info)
    const __stores = {}
    // 存入静态对象，以便于支持保存多个不同的实例。
    this._help[indexedDBFlag] = help // help
    this._stores[indexedDBFlag] = __stores // 仓库变对象

    help._stores = __stores // 挂到 help 上面

    // 把仓库变成对象的形式，避免写字符串的仓库名称
    for (const key in info.stores) {
      __stores[key] = {
        add: (obj, tran = null) => help.addModel(key, obj, tran),
        get: (id = null, tran = null) => help.getModel(key, id, tran),
        count: (tran = null) => help.getCount(key, tran),
        put: (obj, tran = null) => {
          let _id = obj
          if (typeof obj === 'object') {
            _id = obj[info.stores[key].id]
          }
          return help.putModel(key, obj, _id, tran)
        },
        set: (obj, tran = null) => {
          let _id = obj
          if (typeof obj === 'object') {
            _id = obj[info.stores[key].id]
          }
          return help.setModel(key, obj, _id, tran)
        },
        del: (obj, tran = null) => {
          let _id = obj
          if (typeof obj === 'object') {
            _id = obj[info.stores[key].id]
          }
          return help.delModel(key, _id, tran)
        },
        list: (query = {}, tran = null) => help.getList(key, query, tran),
        begin: () => help.beginWrite(key),
        beginWrite: () => help.beginWrite(key),
        beginReadonly: () => help.beginReadonly(key),
        // 给model 加上增删改查的函数
        createModel: (model) => {
          class MyModel {
            constructor (_model) {
              for (const key in _model) {
                this[key] = _model[key]
              }
            }
            /**
             * 添加对象
             * @param {*} tran 事务，可以为 null
             * @returns 
             */
            add(tran = null) {
              return help.addModel(key, this, tran)
            }
            /**
             * 保存对象，无则添加、有则修改
             * @param {*} tran 事务，可以为 null
             * @returns 
             */
            save(tran = null) {
              const _id = this[info.stores[key].id]
              return help.setModel(key, this, _id, tran)
            }
            /**
             * 加载对象，获取对象
             * @param {*} tran 事务，可以为 null
             */
            load(tran = null) {
              return new Promise((resolve, reject) => {
                // 套个娃
                const _id = this[info.stores[key].id]
                help.getModel(key, _id, tran).then((res) => {
                  Object.assign(this, res)
                  resolve(res)
                })
              })
            }
            /**
             * 删除对象
             * @param {*} tran 事务，可以为 null
             * @returns 
             */
            del(tran = null) {
              const _id = this[info.stores[key].id]
              return help.delModel(key, _id, tran)
            }
          }
         
          const re = new MyModel(model)
          return reactive(re)
        }
      }
    }

    return help
  },

  // 获取静态对象里的数据库实例
  useDBHelp (_dbFlag) {
    const flag = typeof _dbFlag === 'undefined' ?
      this._indexedDBFlag : _dbFlag

    return this._help[flag]
  },
  useStores (_dbFlag) {
    const flag = typeof _dbFlag === 'undefined' ?
      this._indexedDBFlag : _dbFlag
    
    return this._stores[flag]
  }
}
