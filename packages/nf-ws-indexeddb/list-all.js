
/**
 * 不分页获取数据，可以查询
 * @param { dbHelp } help 访问数据库的实例
 * @param { Object } storeName 对象仓库
 * @param { Object } findInfo 查询条件
 * @param { Object } pager 排序字段
 * @returns 添加记录的ID
 * * findInfo 结构（查询条件）：
 * * * { colName: [401, 11] } 字段名称，查询方式，查询关键字
 * * pager 结构：
 * * * orderBy: { id: false } // 排序字段：字段名，false表示倒序。
 */
const getList = (help, storeName, findInfo = {}, pager = {}) => {
  const _start = page.start || 0
  const _count = page.count || 0
  const _end = _start + _count
  const _description = pager.description || 'prev' // 默认倒序

  // 查询条件，按照主键或者索引查询
  let keyRange = null
  if (typeof findInfo.indexName !== 'undefined') {
    if (typeof findInfo.indexKind !== 'undefined') {
      const id = findInfo.indexValue
      const dicRange = {
        '=': IDBKeyRange.only(id),
        '>': IDBKeyRange.lowerBound(id, true),
        '>=': IDBKeyRange.lowerBound(id),
        '<': IDBKeyRange.upperBound(id, true),
        '<=': IDBKeyRange.upperBound(id)
      }
      const betweenInfo = findInfo.betweenInfo
      switch (findInfo.indexKind) {
        case '=':
        case '>':
        case '>=':
        case '<':
        case '<=':
          keyRange = dicRange[findInfo.indexKind]
          break
        case 'between':
          keyRange = IDBKeyRange.bound(betweenInfo.v1, betweenInfo.v2, betweenInfo.v1isClose, betweenInfo.v2isClose)
          break
      }
    }
  }
  console.log('findObject - keyRange', keyRange)
  
  return new Promise((resolve, reject) => {
    const _getList = (__tran) => {
      const store = tranRequest.objectStore(storeName)
      let cursorRequest
      // 打开游标
      cursorRequest = store.openCursor(keyRange, _description)

      // 使用游标查询对象并且返回
      cursorRequest.onsuccess = (event) => {
        const cursor = event.target.result
        if (cursor) {
          if (_end === 0 || (cursorIndex >= _start && cursorIndex < _end)) {
            // 判断钩子函数
            if (typeof findInfo.where === 'function') {
              if (findInfo.where(cursor.value, cursorIndex)) {
                dataList.push(cursor.value)
                cursorIndex++
              }
            } else { // 没有设置查询条件
              dataList.push(cursor.value)
              cursorIndex++
            }
          }
          cursor.continue()
        }
        // tranRequest.commit()
      }

      tranRequest.oncomplete = (event) => {
        if (config.debug) {
          console.log('findObjectByIndex - dataList', dataList)
        }
        resolve(dataList)
      }
      tranRequest.onerror = (event) => {
        console.log('findObjectByIndex - onerror', event)
        reject(event)
      }
    }
    // 判断数据库是否打开
    if (tranRequest === null) {
      // 自己开一个事务
      help.beginReadonly([storeName]).then((tran) => {
        _getList(tran)
        tran.commit() // 可以快点提交事务，好吧其实也没快。
      })
    } else {
      _getList(tranRequest)
    }
  })
}

export default getList