
import _toIndex from './_toIndex.js'

/**
 * 用索引查询数据
 * @param { IndexedDBHelp } help 访问数据库的实例
 * @param { string } storeName 仓库名称（表名）
 * @param { Object } query 查询条件
 * * query: {
 * * * keyName1: [401, xxx], // 第一个是索引，
 * * * keyName2: [402, xxx] // 后面的不算索引
 * * }
 * @param { IDBTransaction } tranRequest 如果使用事务的话，需要传递开启事务时创建的连接对象
 * @returns 要获取的对象
 */
const getList = (help, storeName, query = {}, tranRequest = null) => {
  return new Promise((resolve, reject) => {
    // 定义个函数，便于调用
    const _getList = (__tran) => {
      const store = __tran.objectStore(storeName)
      // 分析查询条件，设置索引和其他条件
      const { range, other, find } = _toIndex(store.indexNames, query)

      const keys = Object.keys(query)
      // 设置索引的查询条件
      // 打开对象仓库或者索引 ？ 不用索引，直接用对象仓库 ： 使用索引
      const dbRequest = range === null ? store : store.index(keys[0])
      // // 直接开游标 : // 带条件的开游标
      const cursor = range === null ? dbRequest.openCursor() : dbRequest.openCursor(range)

      const arr = [] // 返回的记录集
      let i = 0
      cursor.onsuccess = (event) => { // 打开成功
        const res = event.target.result
        console.log('游标：' + i++, res)
        if (res) {
          // 判断其他查询条件
          if (find(other, res.value)) {
            arr.push(res.value)
          }
          res.continue() // 继续下一个
        } else {
          // 没有了
          resolve(arr) // 返回对象
        }
      }
    }
    // 判断数据库是否打开
    if (tranRequest === null) {
      // 自己开一个事务
      help.beginReadonly([storeName]).then((tran) => {
        _getList(tran)
        // tran.commit() // 可以快点提交事务，好吧其实也没快。
      })
    } else {
      _getList(tranRequest)
    }
  })
}
export default getList
