/**
 * 获取分页列表数据
 * @param { IndexedDBHelp } help 访问数据库的实例
 * @param { string } storeName 仓库名称（表名）
 * @param { Object } model 对象（数据记录）
 * @param { IDBTransaction } tranRequest 如果使用事务的话，需要传递开启事务时创建的连接对象
 * @returns 添加记录的ID
 * * meta 结构：
 * * * storeName: '', 对象仓库名
 * * info 结构：
 * * * find: {
 * * * * indexName: 'groupId',
 * * * * indexKind: '=', // '>','>=','<','<=','between',
 * * * },
 * * * pager: {
 * * * * start:开始,
 * * * * count:数量,
 * * * * description:'next'
 * * * * indexValue: 1,
 * * * * betweenInfo: {
 * * * * * v1:1,
 * * * * * v2:2,
 * * * * * v1isClose:true,
 * * * * * v2isClose:true,
 * * * * },
 * * * * where：(object) => {
 * * * * * reutrn true/false
 * * * * }
 * * * }
 */
export default function pager (help, storeName, info, tranRequest = null) {
  const _start = info.pager.start || 0
  const _count = info.pager.count || 0
  const _end = _start + _count
  const _description = info.pager.description || 'prev' // 默认倒序

  // 查询条件，按照主键或者索引查询
  let keyRange = null
  if (typeof info.find.indexName !== 'undefined') {
    if (typeof info.find.indexKind !== 'undefined') {
      const id = info.find.indexValue
      const dicRange = {
        '=': IDBKeyRange.only(id),
        '>': IDBKeyRange.lowerBound(id, true),
        '>=': IDBKeyRange.lowerBound(id),
        '<': IDBKeyRange.upperBound(id, true),
        '<=': IDBKeyRange.upperBound(id)
      }
      const betweenInfo = info.find.betweenInfo || '=='
      switch (findInfo.indexKind) {
        case '=':
        case '>':
        case '>=':
        case '<':
        case '<=':
          keyRange = dicRange[findInfo.indexKind]
          break
        case 'between':
          keyRange = IDBKeyRange.bound(betweenInfo.v1, betweenInfo.v2, betweenInfo.v1isClose, betweenInfo.v2isClose)
          break
      }
    }
  }
  console.log('pager - keyRange', keyRange)

  // 定义一个 Promise 的实例
  return new Promise((resolve, reject) => {
    const dataList = []
    // 定义个函数，便于调用
    const _pager = (__tran) => {
      let cursorIndex = 0
      const store = __tran.objectStore(storeName)
      let cursorRequest
      // 判断是否索引查询
      if (typeof findInfo.indexName === 'undefined') {
        cursorRequest = store.openCursor(keyRange, _description)
      } else {
        cursorRequest = store
          .index(info.find.indexName)
          .openCursor(keyRange, _description)
      }

      cursorRequest.onsuccess = (event) => {
        const cursor = event.target.result
        if (cursor) {
          if (_end === 0 || (cursorIndex >= _start && cursorIndex < _end)) {
            // 判断钩子函数
            if (typeof findInfo.where === 'function') {
              if (findInfo.where(cursor.value, cursorIndex)) {
                dataList.push(cursor.value)
                cursorIndex++
              }
            } else { // 没有设置查询条件
              dataList.push(cursor.value)
              cursorIndex++
            }
          }
          cursor.continue()
        }
        // __tran.commit()
      }

      __tran.oncomplete = (event) => {
        if (config.debug) {
          console.log('oncomplete - pager - dataList', dataList)
        }
        resolve(dataList)
      }
    }
    if (tranRequest === null) {
      help.beginReadonly([storeName]).then((tran) => {
        // 自己开一个事务
        _pager(tran)
      })
    } else {
      // 使用传递过来的事务
      _pager(tranRequest)
    }
  })
}
