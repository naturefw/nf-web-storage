/**
 * 删除对象
 * @param { IndexedDBHelp } help 访问数据库的实例
 * @param { string } storeName 仓库名称（表名）
 * @param { string } id 对象的ID
 * @param { IDBTransaction } tranRequest 如果使用事务的话，需要传递开启事务时创建的连接对象
 * @returns 添加记录的ID
 * * meta 结构：
 * * * tableName: '', 表名
 * * * idKey: 'id', 主键字段名称
 */
export default function deleteData (help, storeName, id, tranRequest = null) {
  // 定义一个 Promise 的实例
  return new Promise((resolve, reject) => {
    // 定义个函数，便于调用
    const _delete = (__tran) => {
      __tran
        .objectStore(storeName) // 获取store
        .delete(id) // 删除一个对象
        .onsuccess = (event) => { // 成功后的回调
          resolve(event.target.result)
        }
    }
    // 判断是否有事务
    if (tranRequest === null) {
      help.beginWrite([storeName]).then((tran) => {
        _delete(tran)
      })
    } else {
      _delete(tranRequest)
    }
  })
}
