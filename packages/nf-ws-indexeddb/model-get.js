
/**
 * 获取对象
 * @param { IndexedDBHelp } help 访问数据库的实例
 * @param { string } storeName 仓库名称（表名）
 * @param { Object } id 对象（提供id）
 * @param { IDBTransaction } tranRequest 如果使用事务的话，需要传递开启事务时创建的连接对象
 * @returns 要获取的对象
 */
export default function getModel (help, storeName, id = null, tranRequest = null) {
  return new Promise((resolve, reject) => {
    // 定义个函数，便于调用
    const _getModel = (__tran) => {
      const store = __tran.objectStore(storeName)
      // console.log('对象仓库--', store)
      // 判断是获取一个，还是获取全部
      const dbRequest = id === null ? store.getAll() : store.get(id)
      // console.log('dbRequest--', dbRequest)

      dbRequest.onsuccess = (event) => { // 成功后的回调
        // console.log('-- 得到数据 --：', window.performance.now())
        resolve(event.target.result) // 返回对象
      }
    }
    // 判断数据库是否打开
    if (tranRequest === null) {
      // 自己开一个事务
      help.beginReadonly([storeName]).then((tran) => {
        _getModel(tran)
        tran.commit() // 可以快点提交事务，好吧其实也没快。
      })
    } else {
      _getModel(tranRequest)
    }
  })
}
