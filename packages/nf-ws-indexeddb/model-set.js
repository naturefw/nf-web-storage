import _vueToObject from './_toObject.js'

/**
 * 添加或者修改对象，先依据ID判断是否有对象，无则添，有则改
 * @param { IndexedDBHelp } help 访问数据库的实例
 * @param { string } storeName 仓库名称（表名）
 * @param { Object } model 对象（数据记录）
 * @param { string } id 对象的ID
 * @param { IDBTransaction } tranRequest 如果使用事务的话，需要传递开启事务时创建的连接对象
 * @returns 添加记录的ID
 */
export default function setData (help, storeName, model, id, tranRequest = null) {
  const _model = _vueToObject(model)
  // 定义一个 Promise 的实例
  return new Promise((resolve, reject) => {
    // 定义个函数，便于调用
    const _set = (__tran) => {
      // 先获取对象，然后修改对象，最后存回去
      const store = __tran.objectStore(storeName) // 获取store
      store.get(id) // 获取对象
        .onsuccess = (event) => { // 成功后的回调
          // 从仓库里提取对象，把修改值合并到对象里面。
          const res = event.target.result
          if (typeof res === 'undefined') {
            // 没有对象添加
            store.add(_model) // 添加对象
              .onsuccess = (event) => { // 成功后的回调
                resolve(event.target.result) // 返回对象的ID
              }
          } else {
            // 修改
            const newObject = {}
            Object.assign(newObject, event.target.result, _model)
            // 修改数据
            store.put(newObject) // 修改对象
              .onsuccess = (event) => { // 成功后的回调
                resolve(event.target.result)
              }
          }
        }
    }
    // 判断是否自带事务
    if (tranRequest === null) {
      help.beginWrite([storeName]).then((tran) => {
        // 自己开一个事务
        _set(tran)
      })
    } else {
      // 使用传递过来的事务
      _set(tranRequest)
    }
  })
}
