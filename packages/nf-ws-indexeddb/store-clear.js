/**
 * 清空仓库 store 里的对象
 * @param { IndexedDBHelp } help 访问数据库的实例
 * @param { string } storeName 仓库名称（表名）
 * @param { IDBTransaction } tranRequest 如果使用事务的话，需要传递开启事务时创建的连接对象
 * @returns
 */
export default function deleteStore (help, storeName, tranRequest = null) {
  // 定义一个 Promise 的实例
  return new Promise((resolve, reject) => {
    // 定义个函数，便于调用
    const _clearStore = (__tran) => {
      __tran
        .objectStore(storeName) // 获取store
        .clear() // 删除store
        .onsuccess = (event) => { // 成功后的回调
          resolve(event) // 返回对象的ID
        }
    }
    if (tranRequest === null) {
      help.beginWrite([storeName]).then((tran) => {
        // 自己开一个事务
        _clearStore(tran)
      })
    } else {
      // 使用传递过来的事务
      _clearStore(tranRequest)
    }
  })
}
