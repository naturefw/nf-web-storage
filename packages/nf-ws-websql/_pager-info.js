/**
 * 内部函数，根据分页信息，设置 分页信息和排序字段
 * @param { object } pager 分页和排序
 * @returns order by 和 limit
 */
const _getPager = (pager) => {
  const re =  {
    orderBy: '', // 分页，可以不设置
    limit: '' // 排序，可以不设置
  }
  // 设置分页 order by 和 limit
  if (typeof pager !== 'undefined') {
    if (typeof pager.pagerIndex !== 'undefined') {
      // 用 limit 实现分页
      const _pageSize = pager.pagerSize || 10
      const index = _pageSize * (pager.pagerIndex - 1)
      re.limit = ` limit ${index}, ${_pageSize} `
    }
    if (typeof pager.orderBy === 'object') {
      // 设置排序字段和方式
      const arr = []
      for (const key in pager.orderBy) {
        const col = key
        const isAsc = pager.orderBy[key] ? ' asc ' : ' desc '
        arr.push(col + ' ' + isAsc)
      }
      re.orderBy = ` order by ${arr.join(',')}`
    }
  }

  return re
}

export default _getPager