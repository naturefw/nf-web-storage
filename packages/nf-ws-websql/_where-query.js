/**
 * 内部函数，根据查询条件，拼接查询用的SQL语句，where 后面的部分。
 * @param {object} query 查询条件
 * @returns where 后面的查询语句
 */
const _getWhereQuery = (query) => {
  // 查询条件
  const findKind = {
    // 字符串
    401: ' {col} = ? ',
    402: ' {col} <> ? ',
    403: ' {col} like ? ',
    404: ' {col} not like ? ',
    405: ' {col} like ? ', // 起始于
    406: ' {col} like ? ', // 结束于
    // 数字
    411: ' {col} = ? ',
    412: ' {col} <> ? ',
    413: ' {col} > ? ',
    414: ' {col} >= ? ',
    415: ' {col} < ? ',
    416: ' {col} <= ? ',
    417: ' {col} between ? and ? ',
    // 日期
    421: ' {col} = ? ',
    422: ' {col} <> ? ',
    423: ' {col} > ? ',
    424: ' {col} >= ? ',
    425: ' {col} < ? ',
    426: ' {col} <= ? ',
    427: ' {col} between ? and ? ',
    // 范围
    441: ' {col} in (?)'
  }
  const _whereCol = [] // 查询字段
  const _whereValue = [] // 查询参数
  // 设置查询条件
  for (const key in query) {
    const val = query[key]
    if (val[1] === '' || val[1] === null) continue
    _whereCol.push(findKind[val[0]].replace('{col}', key))
    switch (val[0]) {
      case 403: // like
      case 404: // not like
        _whereValue.push('%' + val[1] + '%')
        break
      case 405: // like a%
        _whereValue.push(val[1] + '%')
        break
      case 406: // like %a
        _whereValue.push('%' + val[1])
        break
      case 417: // between 数字
      case 427: // between 日期
        _whereValue.push(...val[1])
        break
      case 441: // in
        _whereCol[_whereCol.length - 1] =
          _whereCol[_whereCol.length - 1]
            .replace('?', val[1].map(a => '?').join(','))
        _whereValue.push(...val[1])
        break
      default:
        _whereValue.push(val[1])
        break
    }
  }

  const re = {
    whereQuery: '',
    whereValue: []
  }
  // 如果没有查询添加，设置 1=1 占位
  if (_whereCol.length > 0) {
    re.whereQuery = ` WHERE ${_whereCol.join(' and ')}`,
    re.whereValue = _whereValue
  }
  return re

}

export default _getWhereQuery
