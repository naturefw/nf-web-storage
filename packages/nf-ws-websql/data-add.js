
/**
 * 实现添加数据的功能。拼接 insert 的 SQL语句
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { Object } meta 表、字段
 * @param { Object } model 数据
 * @param { connection } cn 如果使用事务的话，需要传递开启事务时创建的连接对象
 * @returns 添加记录的ID
 * * meta 结构：
 * * * tableName: '', 表名
 * * * cols: { colName: '', ...}
 * * model 结构：
 * * * colName: value
 */
export default function addData (help, meta, model, cn = null) {
  // 拼接添加用的SQL语句，
  // 提交SQL语句
  // console.log('addData，开始运行 :')
  const myPromise = new Promise((resolve, reject) => {
    // 记录字段名称
    const colNames = []
    // 记录字段对应的值
    const colValues = []
    // 记录字段对应的占位符合
    const cols = []
    // 变量对象，记录 key和 value
    const colKeys = meta.cols || model
    for (const key in colKeys) {
      if (key.toLocaleLowerCase() === 'id') continue
      colNames.push(key)
      // 判断类型
      if (typeof model[key] === 'object') {
        colValues.push(JSON.stringify(model[key]))
      } else {
        colValues.push(model[key])
      }
      cols.push('?')
    }

    const sql = `INSERT INTO ${meta.tableName} ( ${colNames.join(',')} ) VALUES ( ${cols.join(',')} )`

    // console.log('insertSQL：', sql)
    help.query(sql, colValues, cn)
      .then((res) => {
        // 成功了，返回给调用者
        resolve(res.insertId)
      })
      .catch((err) => {
        reject(err)
      })
  })
  return myPromise
}
