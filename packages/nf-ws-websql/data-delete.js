
/**
 * 实现删除数据的功能。物理删除，delete from
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { Object } meta 表、字段
 * @param { number|string } id 主键字段值
 * @param { connection } cn 如果使用事务的话，需要传递开启事务时创建的连接对象
 * @returns 添加记录的ID
 * * meta 结构：
 * * * tableName: '', 表名
 * * * idKey: 'id', 主键字段名称
 */
export default function deleteData (help, meta, id, cn = null) {
  // 拼接添加用的SQL语句，
  // 提交SQL语句
  // console.log('deleteData，开始运行 :')
  const myPromise = new Promise((resolve, reject) => {
    const sql = `DELETE FROM ${meta.tableName} WHERE ${meta.idKey}=?`
    console.log('sql-----delete:', sql, id)

    help.query(sql, [id], cn)
      .then((res) => {
        // 成功了，返回给调用者
        resolve(res.rowsAffected)
      })
      .catch((err) => {
        reject(err)
      })
  })
  return myPromise
}
