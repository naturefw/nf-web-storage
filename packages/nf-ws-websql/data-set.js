import add from './data-add'
import update from './data-update'

/**
 * 实现添加/修改数据的功能。没有ID：添加，有ID：修改
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { Object } meta 表、字段
 * @param { Object } model 数据
 * @param { number|string } id 主键字段值
 * @param { connection } cn 如果使用事务的话，需要传递开启事务时创建的连接对象
 * @returns 影响的行数
 * * meta 结构：
 * * * tableName: '', 表名
 * * * idKey: 'id', 主键字段名称
 * * model 结构：
 * * * colName: value
 */
 export default function updateData (help, meta, model, id, cn = null) {
  // 拼接修改用的SQL语句，
  // console.log('updateData，开始运行 :')
  const myPromise = new Promise((resolve, reject) => {
    // 记录字段名称
    const colNames = []
    // 记录字段对应的值
    const colValues = []
    // 变量对象，记录 key和 value
    const colKeys = meta.cols || model
    for (const key in colKeys) {
      colNames.push(key + '=? ')
      if (typeof model[key] === 'object') {
        colValues.push(JSON.stringify(model[key], null, 2))
      } else {
        colValues.push(model[key])
      }
    }
    // 加入查询条件
    colValues.push(id)

    const sql = `SELECT 1 FROM ${meta.tableName} WHERE ${meta.idKey || 'id'}=?`
    
    // console.log('updateSQL：', sql)

    help.query(sql, colValues, cn).then((res) => {
      add(help, meta, model, id, cn).then((res1) => {
        // 成功了，返回给调用者
        resolve(res.rowsAffected)
      })
       
    }).catch((err) => {
      reject(err)
    })
  })
  return myPromise
}
