
import _addModel from './data-add.js' // 添加一个对象
import _updateModel from './data-update.js' // 修改一个对象
import _getModel from './data-get.js' // 获取一个对象
import _deleteModel from './data-delete.js' // 删除一个对象

import _listAll from './list-all.js' // 获取仓库里全部对象
import _listCount from './list-count.js' // 统计对象数量
import _listPager from './list-pager.js' // 分页获取对象

/**
 * 基于 promise 封装 webSQL 的基本操作
 * * 创建数据库连接
 * * 提交SQL给数据库执行，得到返回结果
 * * 共用函数
 * * info 结构：
 * * * dbName: 'test',  // 数据库名称
 * * * ver: '1', // 版本，很重要
 * * * size: '2', // 大小，自动 * 1024 * 1024
 * * * description: '数据库描述'
 */
export default class MySQLHelp {
  constructor (info) {
    if (!window.openDatabase) {
      console.log('您的浏览器不支持 webSQL')
      return
    }
    // 数据库连接信息
    this._info = {
      dbName: info.dbName,
      ver: info.ver,
      size: info.size,
      description: info.infodescription
    }
    // 打开数据库
    this.db = window.openDatabase(
      this._info.dbName,
      this._info.ver,
      this._info.description,
      this._info.size * 1024 * 1024)
    // console.log('\n  db', this.db.version)
  }

  /**
   * 运行 SQL 语句，带参数，返回执行结果
   * @param { string } sql SQL语句
   * @param { array } param 参数
   * @param { object } tran 参数
   * @returns promise
   */
  query (sql, param = [], tran = null) {
    const promise1 = new Promise((resolve, reject) => {
      const _query = (tran) => {
        tran.executeSql(sql, param,
          (tx, results) => { resolve(results) },
          (tx, err) => {
            console.log('query - sql:', sql, param, tx, err)
            reject(err)
            return true // 回滚
          }
        )
      }
      if (tran === null) {
        this.begin().then((_tran) => {
          _query(_tran)
        })
      } else {
        _query(tran)
      }
    })
    return promise1
  }

  /**
   * 开启一个事务，Promise 的方式
   * @returns Promise 形式
   */
  begin () {
    const myPromise = new Promise((resolve, reject) => {
      // 开启一个事务。
      // console.log('★ 开启事务，promise 模式')
      this.db.transaction(
        (tran) => { resolve(tran) },
        (tx, err) => { reject(err) }
      )
    })
    return myPromise
  }

  /**
   * 提交一个事务
   * @param { connection } tran 开启事务时创建的连接对象
   * @returns 提交事务
   */
  commit (tran) {
    const myPromise = new Promise((resolve, reject) => {
      // 提交事务
      tran.commit((err) => {
        if (err) {
          console.log('事务提交失败', err)
          reject(err)
        } else {
          resolve()
        }
      })
    })
    return myPromise
  }

  /**
   * 关闭数据库
   * @param { connection } tran 开启事务时创建的连接对象
   */
  close (tran = null) {
    if (tran !== null) {
      // 归还连接对象。console.log('--close: tran', tran.threadId)
      tran.release()
      // console.log('\n[MySQL 事务，已经关闭数据库：] \n')
    } else {
      // 关闭连接
      this.db.end((err) => {
        if (err) {
          console.error('关闭连接发生错误：', err)
        } else {
          // console.log('\n[MySQL 已经关闭数据库：]\n')
        }
      })
    }
  }

  /**
   * 添加一条记录
   * @param {string} meta 表名
   * @param {object} model 要添加的记录
   * @param {*} tran 事务，可以为null
   * @returns
   */
  addModel (meta, model, tran = null) {
    return _addModel(this, meta, model, tran)
  }

  /**
   * 修改一条记录
   * @param {string} meta 表名
   * @param {object} model 要添加的记录
   * @param {*} tran 事务，可以为null
   * @returns
   */
  updateModel (meta, model, tran = null) {
    return _updateModel(this, meta, model, tran)
  }

  /**
   * 删除一条记录
   * @param {string} tableName 表名
   * @param {object} model 要添加的记录
   * @param {*} tran 事务，可以为null
   * @returns
   */
  delModel (meta, model, tran = null) {
    return _deleteModel(this, meta, model, tran)
  }

  /**
   * 获取一条记录
   * @param {string} tableName 表名
   * @param {object} model 要添加的记录
   * @param {*} tran 事务，可以为null
   * @returns
   */
  getModel (meta, model, tran = null) {
    return _getModel(this, meta, model, tran)
  }

  /**
   * 获取全部记录，可以查询
   * @param {string} tableName 表名
   * @param {object} model 要添加的记录
   * @param {*} tran 事务，可以为null
   * @returns
   */
  listAll (meta, query, pager = {}, tran = null) {
    return _listAll(this, meta, query, pager, tran)
  }

  /**
   * 分页获取记录，可以查询
   * @param {string} tableName 表名
   * @param {object} model 要添加的记录
   * @param {*} tran 事务，可以为null
   * @returns
   */
  async listPager (meta, query, pager, _count = 0, tran = null) {
    const count = _count === 0 ? await _listCount(this, meta, query, tran) : _count
    const list = await _listPager(this, meta, query, pager, tran)
    return {
      count,
      list
    }
  }
}

MySQLHelp.prototype.add = _addModel
