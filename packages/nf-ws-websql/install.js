
import webSQL from './help.js'
// 维护
import createTable from './table-create.js'
import deleteTable from './table-delete.js'

export default {
  _webSQLFlag: Symbol('nf-webSQL-help'),
  _help: {}, // 访问数据库的实例
  _tables: {}, // 把表变成对象

  /**
   * 根据参数创建一个数据库的实例，初始化数据库
   * * 删表、建表、添加默认数据
   * @param {*} info 参数
   * @returns 安装一个Vue的插件
   * * dbFlag: '数据库标识',
   * * dbConfig: { // 连接数据库
   * * *  dbName: 'vite2-blog',
   * * *   ver: 1.0,
   * * *  remarks: '测试用的博客数据库',
   * * *  size: 2
   * * },
   * * init: () => {},
   * * tables: {
   * * *  tableName: ['字段名称', '字段名称']
   * * },
   * * isDeleteOldTable: false, // 是否删除之前的表
   */
  createHelp (info) {
    let webSQLFlag = this._webSQLFlag
    if (typeof info.dbFlag === 'string') {
      webSQLFlag = Symbol.for(info.dbFlag)
    } else if (typeof info.dbFlag === 'symbol') {
      webSQLFlag = info.dbFlag
    }
    const init = info.init
    // const tables = this._tables
    // 连接数据库
    // eslint-disable-next-line new-cap
    const help = new webSQL(info.dbConfig)

    // 遍历配置，设置表的操作。
    // 按照表（对象）的配置信息，设置操作实例
    help.begin().then((cn) => {
      for (const key in info.tables) {
        const tableName = key
        const cols = Object.keys(info.tables[key])
        // 判断要不要删除表
        if (info.isDeleteOldTable) {
          // 删除表
          deleteTable(help, tableName, cn)
        }
        // 建立表
        createTable(help, tableName, cols, cn)
      }
    })

    this._help[webSQLFlag] = help
    
    const newTable = {}
    help._tables = newTable
    this._tables[webSQLFlag] = newTable
    // 把表变成对象
    for (const key in info.tables) {
      const meta = {
        tableName: key,
        cols: info.tables[key]
      }
      newTable[meta.tableName] = {
        add: (model, cn = null) => help.addModel(meta, model, cn),
        get: (id = null, cn = null) => help.getModel(meta, id, cn),
        put: (model, cn = null) => {
          let _id = model
          if (typeof model === 'object') {
            _id = model[info.stores[key].id]
          }
          return help.updateModel(meta, model, _id, cn)
        },
        del: (model, cn = null) => {
          let _id = model
          if (typeof model === 'object') {
            _id = model[info.stores[key].id]
          }
          return help.delModel(meta, _id, cn)
        },
        list: (query = {}, isDesc = false, cn = null) => {
          const pager = {
            orderBy: {}
          }
          pager.orderBy[Object.keys(meta.cols)[0]] = isDesc
          return help.listAll(meta, query, pager, cn)
        },
        pager: (query = {}, _pager = null, _count = 0, cn = null) => {
          const pager = {
            pagerIndex: 1,
            pagerSize: 5,
            pagerTotal: 100,
            orderBy: {}
          }
          pager.orderBy[Object.keys(meta.cols)[0]] = false

          if (_pager !== null){
            Object.assign(pager, _pager)
          }
          return help.listPager(meta, query, pager, _count, cn)
        },
        begin: () => help.begin()
      }

    }

    if (typeof init === 'function') {
      init(help, newTable)
    }

    return {
      // 安装插件，不用 provide 注入了
      install (app, options) {
        // 注入状态，用 symbol 作为标记，避免重名
        // app.provide(webSQLFlag, { help, tables })
        // 调用初始化，给全局状态赋值
      }
    }
  },

  // 获取数据库的连接实例
  useSQLHelp (_dbFlag) {
    let flag = this._webSQLFlag
    if (typeof _dbFlag === 'string') {
      flag = Symbol.for(_dbFlag)
    } else if (typeof _dbFlag === 'symbol') {
      flag = _dbFlag
    }
    return this._help[flag]
  },

  /**
   * 把表变成对象，可以直接用表.add/put/del/get等操作
   * @param {*} _dbFlag 
   * @returns 
   */
  useTables (_dbFlag) {
    let flag = this._webSQLFlag
    if (typeof _dbFlag === 'string') {
      flag = Symbol.for(_dbFlag)
    } else if (typeof _dbFlag === 'symbol') {
      flag = _dbFlag
    }
    return this._tables[flag]
  }
}
