import _getWhereQuery from './_where-query.js'
import _getPager from './_pager-info.js'

/**
 * 不分页获取数据，可以查询
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { Object } meta 表、字段
 * @param { Object } query 查询条件
 * @param { Object } pager 排序字段
 * @returns 添加记录的ID
 * * meta 结构：
 * * * tableName： '', 表名
 * * * cols：{colName: '类型'}, 需要显示的字段
 * * query 结构（查询条件）：
 * * * { colName: [401, 11] } 字段名称，查询方式，查询关键字
 * * pager 结构：
 * * * orderBy: { id: false } // 排序字段：字段名，false表示倒序。
 */
export default function listAll (help, meta, query, pager, cn = null) {
  // console.log('开始获取全部记录 :')
  const myPromise = new Promise((resolve, reject) => {
    // 查询条件和查询参数
    const { whereQuery, whereValue } = _getWhereQuery(query)
    // 设置排序
    const { orderBy } = _getPager(pager)

    // 设置显示的字段
    const showCol = Object.keys(meta.cols)
    if (showCol.length === 0) { showCol.push('*') }

    // 拼接查询语句
    const sql = `SELECT ${showCol.join(',')} FROM ${meta.tableName} ${whereQuery} ${orderBy}`
    // console.log('select-all-sql:', sql, whereValue)

    help.query(sql, whereValue, cn)
      .then((res) => {
        // 添加成功
        // console.log('分页获取记录:', res) 
        resolve(Array.from(res.rows))
      })
      .catch((err) => {
        // 出错了
        console.log('获取全部记录失败了:', err)
        reject(err)
      })
  })
  return myPromise
}
