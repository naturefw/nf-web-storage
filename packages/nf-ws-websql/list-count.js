import _getWhereQuery from './_where-query.js'
import _getPager from './_pager-info.js'

/**
 * 分页获取数据，可以查询
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { Object } meta 表、字段
 * @param { Object } query 查询条件
 * @returns 添加记录的ID
 * * meta 结构：
 * * * tableName： '', 表名
 * * * cols：{colName: '类型'}, 需要显示的字段
 * * query 结构（查询条件）：
 * * * { colName: [401, '查询关键字'] } 字段名称，查询方式，查询关键字
 */
export default function getCount (help, meta, query) {
  return new Promise((resolve, reject) => {
    // 查询条件和查询参数
    const { whereQuery, whereValue } = _getWhereQuery(query)
    // 统计总数
    const sql = `SELECT count(1) as count FROM ${meta.tableName}  ${whereQuery} `
    console.log('count-sql:', sql, whereValue)
    help.query(sql, whereValue).then((re) => {
      resolve(re.rows[0].count)
    }).catch((err) => {
      // 出错了
      console.log('统计总记录数失败了:', err)
      reject(err)
    })
  })
}
