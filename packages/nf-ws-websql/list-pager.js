import _getWhereQuery from './_where-query.js'
import _getPager from './_pager-info.js'

/**
 * 分页获取数据，可以查询
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { Object } meta 表、字段
 * @param { Object } query 查询条件
 * @param { Object } pager 数据
 * @returns 添加记录的ID
 * * meta 结构：
 * * * tableName： '', 表名
 * * * cols：{colName: '类型'}, 需要显示的字段
 * * query 结构（查询条件）：
 * * * { colName: [401, 11] } 字段名称，查询方式，查询关键字
 * * pager 结构：
 * * * pageSize: 20 // 一页显示多少条记录
 * * * orderBy: { id: false } // 排序字段：字段名，false表示倒序。
 * * * pageTotal: 100 // 符合查询条件的总记录数
 * * * pageIndex: 1 // 显示第几页的记录，从 1 开始
 */
export default function listPager (help, meta, query, pager) {
  // console.log('开始分页 :')
  const myPromise = new Promise((resolve, reject) => {
    // 查询条件和查询参数
    const { whereQuery, whereValue } = _getWhereQuery(query)
    // 设置排序和分页
    const { orderBy, limit } = _getPager(pager)

    // 设置显示的字段
    const showCol = Object.keys(meta.cols)
    if (showCol.length === 0) { showCol.push('*') }

    // 拼接查询语句
    const sql = `SELECT ${showCol.join(',')} FROM ${meta.tableName} ${whereQuery} ${orderBy} ${limit}`
    console.log('select-pager-sql:', sql, whereValue)

    help.query(sql, whereValue)
      .then((res) => {
        // 添加成功
        // console.log('分页获取记录:', res)
        resolve(Array.from(res.rows))
      })
      .catch((err) => {
        // 出错了
        console.log('分页获取记录失败了:', err)
        reject(err)
      })
  })
  return myPromise
}
