
/**
 * 实现删除数据的功能。逻辑删除，update set flag = 1
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { string } tableName 表名
 * @param { array } cols 字段名集合，数组
 * @param { connection } cn 如果使用事务的话，需要传递开启事务时创建的连接对象
 * @returns 添加记录的ID
 * * meta 结构：
 * * * tableName: '', 表名
 * * * idKey: 'id', 主键字段名称
 * * * cols: {name: ''},
 */
export default function createTable (help, tableName, cols, cn = null) {
  const myPromise = new Promise((resolve, reject) => {
    // 记录字段名称，不设置类型了。
    let _cols = []
    if (typeof cols.length === 'number') {
      _cols = cols
    } else {
      _cols = Object.keys(cols)
    }
    _cols = _cols.filter(key => key.toLowerCase() !== 'id')

    const sql = `CREATE TABLE IF NOT EXISTS ${tableName} (id INTEGER PRIMARY KEY ASC, ${_cols.join(',')} )`
    // console.log('createSQL：', sql)
    // 调用事务，建立表
    help.query(sql, [], cn)
      .then((res) => {
        resolve(res)
      })
      .catch((err) => {
        console.log('createTable -sql:', sql, err)
        const stack = new Error()
        console.log('createTable -sql:', stack)
        reject(err)
      })
  })
  return myPromise
}
