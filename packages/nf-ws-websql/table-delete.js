
/**
 * 实现删除数据的功能。逻辑删除，update set flag = 1
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { string } tableName 表名
 * @param { connection } cn 如果使用事务的话，需要传递开启事务时创建的连接对象
 * @returns 添加记录的ID
 * * meta 结构：
 * * * tableName: '', 表名
 * * * idKey: 'id', 主键字段名称
 * * * delFlag: 'isDel', 逻辑删除，标记字段名称
 */
export default function deleteTable (help, tableName, cn = null) {
  const myPromise = new Promise((resolve, reject) => {
    const sql = `DROP TABLE ${tableName} `
    // console.log('sql：', sql)
    help.query(sql, [], cn)
      .then((res) => {
        // 成功了，返回给调用者
        resolve(res)
      })
      .catch((err) => {
        console.log('deleteTable - sql:', sql, err)
        reject(err)
      })
  })
  return myPromise
}
