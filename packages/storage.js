// 引入各种函数，便于做成npm包
// indexedDB 部分
import dbHelp from './nf-ws-indexeddb/help.js'
import dbInstall from './nf-ws-indexeddb/install.js'

// webSQL 部分
import sqlHelp from './nf-ws-websql/help.js'
import sqlInstall from './nf-ws-websql/install.js'

// indexedDB 部分
const dbCreateHelp = (info) => dbInstall.createHelp(info)
const useDBHelp = (_dbFlag) => dbInstall.useDBHelp(_dbFlag)
const useStores = (_dbFlag) => dbInstall.useStores(_dbFlag)

// webSQL 部分
const sqlCreateHelp = (info) => sqlInstall.createHelp(info)
const useSQLHelp = (_dbFlag) => sqlInstall.useSQLHelp(_dbFlag)
const useTables = (_dbFlag) => sqlInstall.useTables(_dbFlag)

export {
  // webSQL部分
  sqlHelp,
  sqlCreateHelp,
  useSQLHelp,
  useTables,
  // indexedDB 部分
  dbHelp, // indexedDB 的help
  dbCreateHelp, // 创建help，初始化设置
  useDBHelp, // 组件里获取 help
  useStores // 组件里获取对象仓库，方便实现增删改查
}
