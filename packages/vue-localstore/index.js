
const manageStorage = (type) => {
    // type --- local:  localStorage;
    //          session:sessionStorage
    if (typeof type === 'undefined' || type === '') { type = 'local' }
  
    // 设置值
    const setItem = (key, value) => {
      let v = value
      // 记录value的类型，默认是对象/数组
      let valueType = typeof value
      // 依据类型做序列化
      switch (valueType) {
        case 'object':
          // 判断是不是日期类型
          if (value === null) {
            valueType = 'null'
            v = 'null'
          } else if (value instanceof Date) {
            // 保存数据的时间戳
            valueType = 'date'
            v = value.valueOf()
          } else {
            // 对象、数组
            v = JSON.stringify(value)
          }
          break
        case 'function':
          v = value.toString()
          break
        case 'undefined':
          valueType = 'undefined'
          v = 'undefined'
          break
      }
  
      // 把数据、数据类型和时间戳，一起保存
      const object = {
        valueType: valueType,
        time: new Date().valueOf(), // 时间戳，判断是否过期
        value: v
      }
      v = JSON.stringify(object)
      if (type === 'local') {
        localStorage.setItem(key, v)
      } else {
        sessionStorage.setItem(key, v)
      }
    }
  
    // 获取值
    const getItem = (key) => {
      let str = ''
      // 判断存储方式
      if (type === 'local') {
        str = localStorage.getItem(key)
      } else {
        str = sessionStorage.getItem(key)
      }
      // 判断是否为空
      if (typeof str === 'undefined' || str === null || str === '') {
        return str
      }
  
      // 判断格式是否符合，没有太好的办法，暂时先这样。
      if (str.indexOf('{"valueType":"') === -1) {
        return ''
      }
      console.log('-----------------------------------------')
      console.log('111存储的数据的类型：', typeof str)
      console.log('111存储的数据：', str)
      // 把存储的数据转换为对象
      const object = JSON.parse(str)
      // 取值
      let value = object.value
      // 判断存储之前的类型，做转换
      switch (object.valueType) {
        case 'object':// 对象和数组
          value = JSON.parse(value)
          break
        case 'function': // 不做转换
          // value = object.value
          break
        case 'date': // 日期的时间戳
          value = new Date(value)
          break
        case 'number': // 数字
          value = parseInt(value)
          break
        case 'null':
          value = null
          break
        case 'undefined':
          value = undefined
          break
      }
      console.log('存储的数据的类型：')
      console.log(object.valueType, typeof value)
      console.log(object.valueType, Object.prototype.toString.call(value))
      console.log('存储的数据：', value)
      return value
    }
  
    // removeItem
    const removeItem = (key) => {
      if (type === 'local') {
        localStorage.removeItem(key)
      } else {
        sessionStorage.removeItem(key)
      }
    }
    // clear
    const clear = (key) => {
      if (type === 'local') {
        localStorage.clear()
      } else {
        sessionStorage.clear()
      }
    }
    return {
      setItem,
      getItem,
      removeItem,
      clear
    }
  }